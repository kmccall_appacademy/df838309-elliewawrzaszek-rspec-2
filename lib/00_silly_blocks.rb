def reverser(&block)
  string = block.call
  array = string.split
  result = []

  array.each do |word|
    result << word.reverse
  end

  result.join(" ")

end


def adder(value = 1, &block)
  number = block.call
  value + number
end

def repeater(num = 1, &block)
  num.times do block.call
  end
end
